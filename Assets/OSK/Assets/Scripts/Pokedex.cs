using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pokedex : MonoBehaviour
{
    [SerializeField] private Image m_questionImage;
    [SerializeField] private Text m_questionText;
    [SerializeField] private Text m_answerText;
    [SerializeField] private Text m_clueText;
    [SerializeField] private InputField m_inputField;
    [SerializeField] private Animator m_pikachu;
    [SerializeField] private AudioSource m_audioSource;
    
    public enum QuestionType
    {
        TYPED
    }

    private QuestionType m_questionType;
    private TypedQuestion m_typedQuestion;

    void Start()
    {
        m_pikachu.gameObject.SetActive(false);
        SetupWithQuestion();

    }
    
    public void SetupWithQuestion()
    {
        Question question = GameManager.Instance.GetCurrentQuestion();
        if (question is TypedQuestion)
            SetupWithQuestion((TypedQuestion)question);
    }

    public void SetupWithQuestion(TypedQuestion question)
    {
        m_questionType = QuestionType.TYPED;
        m_typedQuestion = question;

        m_questionImage.sprite = question.screenImage;
        m_questionText.text = question.question;
        m_clueText.text = question.clue;

        PlayAudio();
    }

    public void OnQuestionAnswered()
    {
        switch (m_questionType)
        {
            case QuestionType.TYPED:

                if (m_typedQuestion.answer.ToUpper() == m_answerText.text.ToUpper())
                {
                    Good();
                    GameManager.Instance.GoodAnswer();
                }
                else
                {
                    Bad();
                    GameManager.Instance.BadAnswer();
                 
                }

                break;
        }
    }

    private void Good()
    {
        Shared();
        m_pikachu.Play("Happy");
        m_questionText.text = "CORRECTO!";
    }

    private void Bad()
    {
        Shared();
        m_pikachu.Play("Angry");
        m_questionText.text = "PRUEBA OTRA VEZ!";
    }

    private void Shared()
    {
        m_inputField.text = "";
        
        m_clueText.text = "";
        m_inputField.text = "";
        m_pikachu.gameObject.SetActive(true);
        StartCoroutine(DisablePikachu());
    }

    public void PlayAudio()
    {
        m_audioSource.clip = m_typedQuestion.audio;
        m_audioSource.Play();
    }
    
    void Finished()
    {
        SetupWithQuestion();
    }

    IEnumerator DisablePikachu()
    {
        yield return null;
       
        yield return new WaitForSeconds(2f);
        m_pikachu.gameObject.SetActive(false);
        Finished();
    }
}

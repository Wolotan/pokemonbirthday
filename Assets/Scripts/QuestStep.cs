using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "QuestStep", menuName = "ScriptableObjects/QuestStep", order = 1)]
public class QuestStep : ScriptableObject
{
    
    public Question question;
}

using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;
    
    [ReorderableList] public List<Question> m_questSteps;

    public int CurrentStep = 0;
    
    private void Awake()
    {
        //PlayerPrefs.DeleteAll();
        Instance = this;
       // CurrentStep = PlayerPrefs.GetInt("step", 0);
    }

    public void Start()
    {
       
    }
    
    public void GoodAnswer()
    {
        CurrentStep++;
        PlayerPrefs.SetInt("step", CurrentStep);
    }

    public void BadAnswer()
    {
        
    }

    public void Back()
    {
        
    }

    public Question GetCurrentQuestion()
    {
        return m_questSteps[CurrentStep];
    }
}

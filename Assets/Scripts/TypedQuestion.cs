using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TypedQuestion", menuName = "ScriptableObjects/TypedQuestion", order = 1)]
public class TypedQuestion : Question
{
    public string question;
    public string clue;
    public string answer;
}

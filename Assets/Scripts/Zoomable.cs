using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoomable : MonoBehaviour
{
    public Animator animator;
    private bool zoomed;
    
    public void OnPressed()
    {

        animator.CrossFade(zoomed ? "Idle" : "Big", 0.2f);
        zoomed = !zoomed;
    }
}
